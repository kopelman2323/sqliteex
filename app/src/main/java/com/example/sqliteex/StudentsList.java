package com.example.sqliteex;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

public class StudentsList extends AppCompatActivity {
    private ListView studentsListView;
    private StudentsDBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students_list);
        initWidgets();
    }

    private void initWidgets() {
        this.studentsListView = findViewById(R.id.students_list_view);
        this.dbHelper = new StudentsDBHelper(this);

        List<Student> students = this.dbHelper.getAllStudents();
        ArrayAdapter<Student> studentsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, students);
        this.studentsListView.setAdapter(studentsAdapter);
    }
}