package com.example.sqliteex;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StudentsDBHelper dbHelper = new StudentsDBHelper(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_student:
                addStudent();
                return true;
            case R.id.students_list_view:
                showStudentsList();
                return true;
            case R.id.students_by_name:
                showStudentsByName();
                return true;
            case R.id.students_above_avg:
                showStudentAboveAvg();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showStudentAboveAvg() {
        Intent intent = new Intent(this, StudentAboveAvg.class);
        startActivity(intent);
    }

    private void showStudentsByName() {
        Intent intent = new Intent(this, StudentsByName.class);
        startActivity(intent);
    }

    private void showStudentsList() {
        Intent intent = new Intent(this, StudentsList.class);
        startActivity(intent);
    }

    private void addStudent() {
        Intent intent = new Intent(this, AddStudent.class);
        startActivity(intent);
    }

}