package com.example.sqliteex;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.List;

public class StudentsByName extends AppCompatActivity {
    private EditText firstNameEditText;
    private Button searchButton;
    private ListView studentsListView;
    private StudentsDBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students_by_name);
        initWidgets();
    }

    private void initWidgets() {
        this.dbHelper = new StudentsDBHelper(this);
        this.firstNameEditText = findViewById(R.id.first_name_edit_text);
        this.searchButton = findViewById(R.id.search_button);
        this.studentsListView = findViewById(R.id.students_list_view);
        List<Student> students = this.dbHelper.getAllStudents();
        displayStudents(students);
        this.searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayFilteredStudents();
            }
        });
    }

    private void displayStudents(List<Student> students) {
        ArrayAdapter<Student> studentsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, students);
        this.studentsListView.setAdapter(studentsAdapter);
    }

    private void displayFilteredStudents() {
        String firstName = this.firstNameEditText.getText().toString();
        List<Student> students = this.dbHelper.findStudentsByName(firstName);
        displayStudents(students);
    }
}