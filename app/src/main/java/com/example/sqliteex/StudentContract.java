package com.example.sqliteex;

import android.provider.BaseColumns;

public abstract class StudentContract {
    /* Inner class that defines the table contents */
    public static class StudentEntry implements BaseColumns {
        public static final String TABLE_NAME = "students";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_FIRST_NAME = "firstName";
        public static final String COLUMN_NAME_LAST_NAME = "lastName";
        public static final String COLUMN_NAME_ADDRESS = "address";
        public static final String COLUMN_NAME_AVG = "avg";

        public static final String COLUMN_NAME_AND_TYPE_ID = COLUMN_NAME_ID + " INTEGER PRIMARY KEY";
        public static final String COLUMN_NAME_AND_TYPE_FIRST_NAME = COLUMN_NAME_FIRST_NAME + " TEXT";
        public static final String COLUMN_NAME_AND_TYPE_LAST_NAME = COLUMN_NAME_LAST_NAME + " TEXT";
        public static final String COLUMN_NAME_AND_TYPE_ADDRESS = COLUMN_NAME_ADDRESS + " TEXT";
        public static final String COLUMN_NAME_AND_TYPE_AVG = COLUMN_NAME_AVG + " INTEGER";
    }
}
