package com.example.sqliteex;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class AddStudent extends AppCompatActivity {
    private EditText idEditText, firstNameEditText, lastNameEditText, addressEditText, avgEditText;
    private Button addStudentButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);
        initWidgets();
    }

    private void initWidgets() {
        this.idEditText = findViewById(R.id.id_edit_text);
        this.firstNameEditText = findViewById(R.id.first_name_edit_text);
        this.lastNameEditText = findViewById(R.id.last_name_edit_text);
        this.addressEditText = findViewById(R.id.address_edit_text);
        this.avgEditText = findViewById(R.id.avg_edit_text);
        this.addStudentButton = findViewById(R.id.add_student_button);
        this.addStudentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StudentsDBHelper dbHelper = new StudentsDBHelper(AddStudent.this);
                dbHelper.insertStudent(generateStudentFromEditTexts());
                finish();
            }
        });
    }

    private Student generateStudentFromEditTexts() {
        long id = Long.parseLong(this.idEditText.getText().toString());
        String firstName = this.firstNameEditText.getText().toString();
        String lastName = this.lastNameEditText.getText().toString();
        String address = this.addressEditText.getText().toString();
        int avg = Integer.parseInt(this.avgEditText.getText().toString());
        return new Student(id, firstName, lastName, address, avg);
    }
}