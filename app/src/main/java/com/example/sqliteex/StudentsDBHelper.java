package com.example.sqliteex;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import static com.example.sqliteex.StudentContract.StudentEntry.COLUMN_NAME_ADDRESS;
import static com.example.sqliteex.StudentContract.StudentEntry.COLUMN_NAME_AND_TYPE_ADDRESS;
import static com.example.sqliteex.StudentContract.StudentEntry.COLUMN_NAME_AND_TYPE_AVG;
import static com.example.sqliteex.StudentContract.StudentEntry.COLUMN_NAME_AND_TYPE_FIRST_NAME;
import static com.example.sqliteex.StudentContract.StudentEntry.COLUMN_NAME_AND_TYPE_ID;
import static com.example.sqliteex.StudentContract.StudentEntry.COLUMN_NAME_AND_TYPE_LAST_NAME;
import static com.example.sqliteex.StudentContract.StudentEntry.COLUMN_NAME_AVG;
import static com.example.sqliteex.StudentContract.StudentEntry.COLUMN_NAME_FIRST_NAME;
import static com.example.sqliteex.StudentContract.StudentEntry.COLUMN_NAME_ID;
import static com.example.sqliteex.StudentContract.StudentEntry.COLUMN_NAME_LAST_NAME;
import static com.example.sqliteex.StudentContract.StudentEntry.TABLE_NAME;

public class StudentsDBHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Students.db";
    private static final String SQL_CREATE_ENTRIES =
            String.format("CREATE TABLE %s ( %s, %s, %s, %s, %s)",
                    TABLE_NAME,
                    COLUMN_NAME_AND_TYPE_ID,
                    COLUMN_NAME_AND_TYPE_FIRST_NAME,
                    COLUMN_NAME_AND_TYPE_LAST_NAME,
                    COLUMN_NAME_AND_TYPE_ADDRESS,
                    COLUMN_NAME_AND_TYPE_AVG);

    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + TABLE_NAME;

    public StudentsDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public long insertStudent(Student student) {
        // Gets the data repository in write mode
        SQLiteDatabase db = this.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = getContentValuesWithId(student);

        // Insert the new row, returning the primary key value of the new row
        long newStudentId = db.insert(TABLE_NAME, null, values);
        return newStudentId;
    }

    private ContentValues getContentValuesWithId(Student student) {
        ContentValues values = getContentValuesWithoutId(student);
        values.put(COLUMN_NAME_ID, student.getId());
        return values;
    }

    private ContentValues getContentValuesWithoutId(Student student) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_FIRST_NAME, student.getFirstName());
        values.put(COLUMN_NAME_LAST_NAME, student.getLastName());
        values.put(COLUMN_NAME_ADDRESS, student.getAddress());
        values.put(COLUMN_NAME_AVG, student.getAvg());
        return values;
    }

    public List<Student> findStudentsByName(String studentFirstName) {
        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {"*"};

        // Filter results WHERE "title" = 'My Title'
        String selection = COLUMN_NAME_FIRST_NAME + " = ?";
        String[] selectionArgs = {studentFirstName};

        return getStudents(db, projection, selection, selectionArgs);
    }

    public List<Student> findStudentsWithGreaterAvg(int studentAvg) {
        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {"*"};

        // Filter results WHERE "title" = 'My Title'
        String selection = COLUMN_NAME_AVG + " > ?";
        String[] selectionArgs = {String.valueOf(studentAvg)};

        return getStudents(db, projection, selection, selectionArgs);
    }

    public boolean deleteStudent(long studentId) {
        // Define 'where' part of query.
        String selection = COLUMN_NAME_ID + " LIKE ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = {String.valueOf(studentId)};
        // Issue SQL statement.
        int deletedRows = this.getWritableDatabase().delete(TABLE_NAME, selection, selectionArgs);
        return deletedRows == 1;
    }

    public boolean updateStudent(Student student) {
        SQLiteDatabase db = this.getWritableDatabase();

        // New value for one column
        ContentValues values = getContentValuesWithoutId(student);

        // Which row to update, based on the title
        String selection = COLUMN_NAME_ID + " LIKE ?";
        String[] selectionArgs = {String.valueOf(student.getId())};

        int count = db.update(TABLE_NAME, values, selection, selectionArgs);
        return count == 1;
    }

    public List<Student> getAllStudents() {
        SQLiteDatabase db = this.getReadableDatabase();
        return getStudents(db, new String[]{"*"}, null, null);
//        List<Student> students = new ArrayList<>();
//        for (int i = 1; i <= 100; i++) {
//            students.add(new Student(i, "name-" + i, "lastName-" + i, "address-" + i, i * 20));
//        }
//        return students;
    }

    private List<Student> getStudents(SQLiteDatabase db, String[] projection, String selection, String[] selectionArgs) {
        Cursor cursor = db.query(
                TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                null               // The sort order
        );
        List<Student> students = new ArrayList<>();
        while (cursor.moveToNext()) {
            students.add(getStudentFromCursor(cursor));
        }
        cursor.close();
        return students;
    }

    private Student getStudentFromCursor(Cursor cursor) {
        long id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_NAME_ID));
        String firstName = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_FIRST_NAME));
        String lastName = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_LAST_NAME));
        String address = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_ADDRESS));
        int avg = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_NAME_AVG));
        return new Student(id, firstName, lastName, address, avg);
    }

}
